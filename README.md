# iperf3 Docker container (Test Network Speed From Host To Host)

## Install Docker and Docker-compose

### Windows

https://docs.docker.com/docker-for-windows/install/


### Linux


- [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
- [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
- [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
- [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

Install docker-compose.

https://docs.docker.com/compose/install/

## Run it

```shell
git clone https://gitlab.com/Defria/iperf.git
cd iperf3
docker-compose build --no-cache
docker-compose up
# docker-compose up --build
```

## Test Network Speed From Host To Host
> Download client https://iperf.fr/iperf-download.php
```shell
> iperf3 -c <host IP address> -p 50001
```

## Customize it

`vim docker-compose.yml`

```yaml
...
    ports:
      - "<your preferred port>:5001/tcp"
      - "<your preferred port>:5001/udp"
...
    command: iperf3 -s <options> -p 5001
...
```

Done!

